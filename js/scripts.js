$(function() {
    //set view to 100% window height
    var $view = $(".view");
    $view.height($(window).height());
    $(window).resize(function() {
        $view.height($(window).height());
    });

    $('body').fadeIn(1000);

    /* introduction view javascript */
    var $introView = $(".introduction-view");
    fadeElements($introView.find(".content .item"), false);
    fadeElements($(".idea").find(".centre-text .item"), true);

    $introView.find(".next-page").on("click", function(e) {
        $introView.find("#snowboard").addClass("snowboard-out");
        e.preventDefault();
    });

    /* project view javascript */
    var projectsText = [
        "Currently I work full time as a frontend developer",
        "I have launched my own hybrid app",
        "and have worked on many websites"
    ];

    var projectIndex = 0;
    var $projectsText = $("#projects-text");
    $projectsText.find(".project-back").hide();
    $projectsText.find(".text p").text(projectsText[projectIndex]).addClass('text-typing-1');

    var $projectBack = $(".project-back");
    var $projectNext = $(".project-next");

    $projectBack.on("click", function(e) {
        projectIndex = projectIndex - 1;
        setProjectCounter(projectIndex);
        showProjectCard(projectIndex);
    });

    $projectNext.on("click", function(e) {
        projectIndex = projectIndex + 1;
        $projectsText.find(".text p").text(projectsText[projectIndex]).addClass('text-typing-' + (projectIndex + 1));
        setProjectCounter(projectIndex);
        showProjectCard(projectIndex);
    });

    function setProjectCounter(projectIndex) {
        //hide back button if first text
        if (projectIndex == 0) {
            $projectsText.find(".project-back").hide();
        //hide next button if last text
        } else if (projectIndex == projectsText.length - 1) {
            $projectsText.find(".project-next").hide();
            var $lightbulb = $("#lightbulb");
            if (!$lightbulb.hasClass("wobble")) {
                setTimeout(function () {
                    $lightbulb.addClass("wobble visible");
                    //$("#lightbulb-holder")
                }, 1000);
            }
        //show both
        } else {
            $projectsText.find(".project-next").show();
            $projectsText.find(".project-back").show();
        }
        $projectsText.find(".text p").text(projectsText[projectIndex]);
    }

    function showProjectCard(projectIndex) {
        if (projectIndex !== 0) {
            var $projectCards = $('#projects').find('.project');
            $($projectCards[projectIndex]).show();
        }
    }

    var $projectCards = $('#projects').find('.project');
    var woosh = $("#woosh")[0];
    $.each($projectCards, function(){
        $(this).mouseenter(function() {
            woosh.pause();
            woosh.play();
        });
    });

    // $($projectCards[projectIndex]).mouseenter(function() {
    //         woosh.pause();
    //         woosh.play();
    //     });

    var $lightbulb = $("#lightbulb");
    if ($lightbulb.length > 0) {
        $lightbulb.draggable({
            start: function() {
                $("#lightbulb-holder").addClass("wobble");
                $("#lightbulb").removeClass("wobble");
            },
            stop: function() {
                $("#lightbulb-holder").removeClass("wobble");
                $("#lightbulb").addClass("wobble");
            },
            revert: 'invalid'
        });
        $("#lightbulb-holder").droppable({
            accept: "#lightbulb",
            drop: function (event, ui) {
                $lightbulb.removeClass('wobble');
                $lightbulb.addClass("yellow");
                $("body").fadeOut(1000, function() {
                    window.location = "prior-learning.html";
                });
            }
        });
    }

    /* other custom javascript */
    //next page click
    $(".next-page").find("a").on("click", function(e) {
        e.preventDefault();
        console.log(this);
        var nextPage = this.href;
        console.log(nextPage);
        $("body").fadeOut(1000, function() {
            window.location = nextPage;
        });
    });

    //documents section waypoints
    var $documentSection = $('section.documents');
    if ($documentSection.length > 0) {
        $documentSection.waypoint({
            handler: function() {
                var $element = $(this.element);
                $element.find(".item.project").addClass('fade-in-out');
                $element.find(".button.project").addClass('fade-in');
            },
            offset: '30%'
        });

        $documentSection.waypoint({
            handler: function() {
                var $element = $(this.element);
                $element.find(".item.document").addClass('fade-in-out');
                $(".button.document").addClass('fade-in');
            },
            offset: '-10%'
        });

        $documentSection.waypoint({
            handler: function() {
                var $element = $(this.element);
                $element.find(".item.website").addClass('fade-in-out');
                $(".button.website").addClass('fade-in');
            },
            offset: '-40%'
        });

        $documentSection.waypoint({
            handler: function() {
                var $element = $(this.element);
                $element.find(".item.app").addClass('fade-in-out');
                $(".buttons").find(".button.app").addClass('fade-in');
            },
            offset: '-70%'
        });

        $documentSection.waypoint({
            handler: function() {
                var $element = $(this.element);
                $element.find("h1.item").addClass('fade-in');
                $element.find("p.item").addClass('fade-in');
            },
            offset: '-80%'
        });
    }

    /* helper functions */
    function fadeElements($elements, fadeOut) {
        $.each($elements, function(index, element) {
            var $element = $(element);

            //fade in
            setTimeout(function () {
                $element.animate({
                    opacity: 1
                }, 2000);
            }, 1000 + (index * 3000));

            //fade out
            if (fadeOut && (index !== ($elements.length - 2 || $elements.length - 1))) {
                setTimeout(function () {
                    $element.animate({
                        opacity: 0
                    }, 2000);
                }, 2000 + (index * 3000));
            }
        });
    }
});

//window loader
$(window).on("load", function() {
    console.log("load");
    $(".loader").fadeOut("slow");
});