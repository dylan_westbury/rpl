$(document).ready(function() {
    $(window).Scrollax();

    var height = $(document).height() - $(window).height();

    $(window).scroll(function() {
        if ($(document).scrollTop() === height) {
            $(".project-next").show();
            $(".project-back").show();
        }
    });
});